# Authentication

There are two types of tokens that can be used to authenticate with the system.
  1. `App Tokens` - Readonly token that is linked to a particular app. This
token can only be used to access the posts under said app using the [Client
API](#client-api).
  2. `Private Tokens` - Linked to a user's account and thus is able to modify any 
aspect of said account using the [Management API](#management-api).


# Client API

An app token MUST be used to authenticate with the sever by using the
`Authorization` Header with the custom authentication scheme `Herd-App-Token`.

```
Authorization: Herd-App-Token <token>
```

| Method | Route |
|--------|-------|
| GET    | `/v0/posts` |
| GET    | `/v0/posts/:postId` |

## List posts in vicinity

```
GET /v0/posts
```
> :exclamation: This route will probably be renamed

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `lat`     | float   | yes      | Latitude part of GPS coodinate |
| `lng`     | float   | yes      | Longitude part of GPS coordinate |
| `page`    | int     | no       | page number (for pagination) |
| `perPage` | int     | no       | number of items returned per page. min: 10, max: 50. Defaults to 25 |

```
curl --header "Authorization: Herd-App-Token <your_app_token>" \
  "https://<herd-api.com>/v0/posts?lat=31.8856&lng=168.1435"
```

Example of response

```json
{
  "data": {
    "posts": [
      {
        "id": "9d88e7b0-c4d1-4474-9470-590b5aeb9de7",
        "createdAt": "2019-09-02T09:41:21.165Z",
        "updatedAt": "2019-09-02T09:41:21.165Z",
        "lat": 31.8858,
        "lng": 168.1437,
        "distance": 1400,
        "priority": 3,
        "title": "10% off on select items at @place"
      },
      ...
    ],
    "count": 25,
    "page": 1,
    "total": 32
  }
}
```
> The response will be sorted by distance. The closest item will be the first
> item on the list and subsequently the farthest item will be the last item on
> the list. However the priority field overrides this behavior and higher
> priority items (smaller numbers) will rank higer regardless of their distance.

## Get a single post

```
GET /v0/posts/:postId
```
> :exclamation: This route will probably be renamed

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `postId`  | string   | yes      | id of a particular post |

```
curl --header "Authorization: Herd-App-Token <your_app_token>" \
  "https://<herd-api.com>/v0/posts/9d88e7b0-c4d1-4474-9470-590b5aeb9de7"
```

Example of response

```json
{
  "data": {
    "id": "9d88e7b0-c4d1-4474-9470-590b5aeb9de7",
    "createdAt": "2019-09-02T09:41:21.165Z",
    "updatedAt": "2019-09-02T09:41:21.165Z",
    "body": "long string body ........................"
  }
}
```


# Management API

A private token MUST be used to authenticate with the sever by using the
`Authorization` Header with the custom authentication scheme
`Herd-Private-Token`.

```
Authorization: Herd-Private-Token <token>
```

## Manage Private Tokens

| Method | Route |
|--------|-------|
| GET    | `/v0/user/privateTokens` |
| POST   | `/v0/user/privateTokens` |
| DELETE | `/v0/user/privateTokens/:privateTokenId` |

### List Private Tokens

> :exclamation: The token itself will not appear in the response.
> The token appears in the respone only at creation.

```
GET /v0/user/privateTokens
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| - | - | - | - |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  "https://<herd-api.com>/v0/user/privateTokens"
```

Example of response

```json
{
  "data": {
    "privateTokens": [
      {
        "id": "b05ca75d-1400-441c-83b4-2f85783318a7",
        "createdAt": "2019-09-02T09:41:20.588Z",
        "updatedAt": "2019-09-02T09:41:20.588Z",
        "name": "Private Token for My Web portal backend"
      },
      ...
    ],
    "count": 2,
    "page": 1,
    "total": 2
  }
}
```

### Create Private Token

> :exclamation: This is the only time where the token will appear in the api response.

```
POST /v0/user/privateTokens
```

| Attribute | Type    | Required | Description         |
| --------- | ------- | -------- | ------------------- |
| `name`    | string | yes | Name of the token. Limited to 50 characters |

```
curl --request POST --header "Authorization: Herd-Private-Token <your_private_token>" \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"Private Token for custom backend" }' \
  "https://<herd-api.com>/v0/user/privateTokens"
```

Example of response

```json
{
  "data": {
    "id": "b05ca75d-1400-441c-83b4-2f85783318a7",
    "createdAt": "2019-09-02T09:41:20.588Z",
    "updatedAt": "2019-09-02T09:41:20.588Z",
    "name": "Private Token for custom backend",
    "token": "~6XReWzxEwlth.3e2THM4u-U4pKbrU8bdlASqaqT"
  }
}
```

### Delete a private token

> :exclamation: Be carefull not to delete the token in use.

```
DELETE /v0/user/privateTokens/:privateTokenId
```

| Attribute        | Type    | Required | Description         |
| ---------------- | ------- | -------- | ------------------- |
| `privateTokenId` | string  | yes      | id of a private Token |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  --request DELETE \
  "https://<herd-api.com>/v0/user/privateTokens/544cf1a7-aaf1-4909-bf50-03c2707697f3"
```

Example of response

```json
{ }
```


## Manage Apps

| Method | Route |
|--------|-------|
| GET    | `/v0/apps` |
| POST   | `/v0/apps` |
| PUT    | `/v0/apps/:appId` |
| DELETE | `/v0/apps/:appId` |


### List apps

```
GET /v0/apps
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `page`    | int     | no       | page number (for pagination) |
| `perPage` | int     | no       | number of items returned per page. min: 5, max: 20. Defaults to 20 |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  "https://<herd-api.com>/v0/apps"
```

Example of response

```json
{
  "data": {
    "apps": [
      {
        "id": "2afc7823-644e-4c52-8069-48531f6b5c1a",
        "createdAt": "2019-09-02T09:41:20.588Z",
        "updatedAt": "2019-09-02T09:41:20.588Z",
        "name": "My Food App",
        "description": "app description"
      },
      ...
    ],
    "count": 2,
    "page": 1,
    "total": 2
  }
}
```

### Create an app

```
POST /v0/apps
```

| Attribute     | Type    | Required | Description         |
| ------------- | ------- | -------- | ------------------- |
| `title`       | string  | yes      | Name of the app |
| `description` | string  | no       | App description |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"title":"App Name","description":"foo bar"}' \
  "https://<herd-api.com>/v0/apps"
```

Example of response

```json
{
  "data": {
    "id": "2afc7823-644e-4c52-8069-48531f6b5c1a",
    "createdAt": "2019-09-02T09:41:20.588Z",
    "updatedAt": "2019-09-02T09:41:20.588Z",
    "name": "App Name",
    "description": "foo bar"
  }
}
```

### Update an app

```
PUT /v0/apps/:appId
```

| Attribute     | Type    | Required | Description         |
| ------------- | ------- | -------- | ------------------- |
| `appId`       | string  | yes      | id of an app |
| `title`       | string  | yes      | Name of the app |
| `description` | string  | yes      | App description |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  --header "Content-Type: application/json" \
  --request PUT \
  --data '{"title":"Fruits","description":"Salad"}' \
  "https://<herd-api.com>/v0/apps/2afc7823-644e-4c52-8069-48531f6b5c1a"
```

Example of response

```json
{
  "data": {
    "id": "2afc7823-644e-4c52-8069-48531f6b5c1a",
    "createdAt": "2019-09-02T09:41:20.588Z",
    "updatedAt": "2019-09-02T09:41:20.588Z",
    "name": "FRUIT",
    "description": "SALAD"
  }
}
```

### Delete an app

> Deleting an app will also delete all posts associated with it

```
DELETE /v0/apps/:appId
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `appId`   | string  | yes      | id of an app |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  --request DELETE \
  "https://<herd-api.com>/v0/apps/2afc7823-644e-4c52-8069-48531f6b5c1a"
```

Example of response

```json
{ }
```


## Manage Posts

| Method | Route |
|--------|-------|
| GET    | `/v0/apps/:appId/posts` |
| POST   | `/v0/apps/:appId/posts` |
| PUT    | `/v0/apps/:appId/posts/:postId` |
| DELETE | `/v0/apps/:appId/posts/:postId` |

### List posts

```
GET /v0/apps/:appId/posts
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `appId`   | string  | yes      | id of an app |
| `page`    | int     | no       | page number (for pagination) |
| `perPage` | int     | no       | number of items returned per page. min: 10, max: 50. Defaults to 50 |

```
curl --header "Authorization: Herd-App-Token <your_private_token>" \
  "https://<herd-api.com>/v0/apps/9d88e7b0-c4d1-4474-9470-590b5aeb9de7"
```

Example of response

```json
{
  "data": {
    "posts": [
      {
        "id": "9d88e7b0-c4d1-4474-9470-590b5aeb9de7",
        "createdAt": "2019-09-02T09:41:21.165Z",
        "updatedAt": "2019-09-02T09:41:21.165Z",
        "lat": 31.8858,
        "lng": 168.1437,
        "title": "10% off on select items at @place",
        "body": "long string ........",
        "range" 3,
        "priority": 4
      },
      ...
    ],
    "count": 7,
    "page": 1,
    "total": 7
  }
}
```
### Create a post

```
POST /v0/apps/:appId/posts
```

| Attribute  | Type    | Required | Description         |
| ---------- | ------- | -------- | ------------------- |
| `appId`    | string  | yes      | id of an app |
| `lat`      | float   | yes      | Latitude part of GPS coodinate |
| `lng`      | float   | yes      | Longitude part of GPS coordinate |
| `title`    | string  | yes      | Title of the post. Limited to 256 characters |
| `body`     | string  | no       | Body of the post. Limited to 2500 characters |
| `range`    | integer | no       | Range (in Kilometers) of the post. Must to be one of 3, 5 or 10. Defaults to 3. |
| `priority` | integer | no       | Lower numbers indicate high priority. min: 1, max: 10. Defaults to 5. |

```
curl --header "Authorization: Herd-App-Token <your_private_token>" \
   --header "Content-Type: application/json" \
  --request POST \
  --data '{"lat": 31.8858,"lng": 168.1437, "title": "Some title", "range": 5}' \
  "https://<herd-api.com>/v0/apps/9d88e7b0-c4d1-4474-9470-590b5aeb9de7/posts"
```

Example of response

```json
{
  "data": {
    "id": "9d88e7b0-c4d1-4474-9470-590b5aeb9de7",
    "createdAt": "2019-09-02T09:41:21.165Z",
    "updatedAt": "2019-09-02T09:41:21.165Z",
    "lat": 31.8858,
    "lng": 168.1437,
    "title": "Some title",
    "body": "",
    "range" 5,
    "priority": 5
    }
}
```

### Update a post

```
PUT /v0/apps/:appId/posts/:postId
```

| Attribute  | Type    | Required | Description         |
| ---------  | ------- | -------- | ------------------- |
| `appId`    | string  | yes      | id of an app |
| `appId`    | string  | yes      | id of a post |
| `lat`      | float   | yes      | Latitude part of GPS coodinate |
| `lng`      | float   | yes      | Longitude part of GPS coordinate |
| `title`    | string  | yes      | Title of the post. Limited to 256 characters |
| `body`     | string  | yes      | Body of the post. Limited to 2500 characters |
| `range`    | integer | yes      | Range (in Kilometers) of the post. Must to be one of 3, 5 or 10. Defaults to 3. |
| `priority` | integer | yes       | Lower numbers indicate high priority. min: 1, max: 10. Defaults to 5. |

```
curl --header "Authorization: Herd-App-Token <your_private_token>" \
   --header "Content-Type: application/json" \
  --request PUT \
  --data '{"lat": 31.8858,"lng": 168.1437, "title": "Some title", body: "foo bar" "range": 3, "priority": 1 }' \
  "https://<herd-api.com>/v0/apps/9d88e7b0-c4d1-4474-9470-590b5aeb9de7/posts/b3ae2a77-d502-499e-a96d-d656cb467bf2"
```

Example of response

```json
{
  "data": {
    "id": "b3ae2a77-d502-499e-a96d-d656cb467bf2",
    "createdAt": "2019-09-02T09:41:21.165Z",
    "updatedAt": "2019-09-02T09:41:21.165Z",
    "lat": 31.8858,
    "lng": 168.1437,
    "title": "Some title",
    "body": "foo bar",
    "range" 3,
    "priority": 1
  }
}
```
### Delete a post

```
DELETE /v0/apps/:appId/posts/:postId
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `appId`   | string  | yes      | id of an app |
| `postId`   | string  | yes      | id of a post |

```
curl --header "Authorization: Herd-App-Token <your_private_token>" \
  --request DELETE \
  "https://<herd-api.com>/v0/apps/9d88e7b0-c4d1-4474-9470-590b5aeb9de7/posts/b3ae2a77-d502-499e-a96d-d656cb467bf2"
```

Example of response

```json
{ }
```

## Manage App Tokens

| Method | Route |
|--------|-------|
| GET    | `/v0/apps/:appId/appTokens` |
| POST   | `/v0/apps/:appId/appTokens` |
| DELETE | `/v0/apps/:appId/appTokens/:appTokenId` |

### List App Tokens

> :exclamation: The token itself will not appear in the response.
> The token appears in the respone only at creation.

```
GET /v0/apps/:appId/appTokens
```

| Attribute | Type    | Required | Description         |
|-----------|---------|----------|---------------------|
| `appId`   | string  | yes | id of an app |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  "https://<herd-api.com>/v0/apps/d0717902-d059-4276-b02f-4ff44d9264f5/appTokens"
```

Example of response

```json
{
  "data": {
    "appTokens": [
      {
        "id": "b05ca75d-1400-441c-83b4-2f85783318a7",
        "createdAt": "2019-09-02T09:41:20.588Z",
        "updatedAt": "2019-09-02T09:41:20.588Z",
        "name": "App token for Production App"
      },
      ...
    ],
    "count": 2,
    "page": 1,
    "total": 2
  }
}
```

### Create App Token

> :exclamation: This is the only time where the token will appear in the api response.

```
POST /v0/apps/:appId/appTokens
```

| Attribute | Type    | Required | Description         |
| --------- | ------- | -------- | ------------------- |
| `appId`   | string  | yes | id of an app |
| `name`    | string  | yes | Name of the token. Limited to 50 characters |

```
curl --request POST --header "Authorization: Herd-Private-Token <your_private_token>" \
  --header "Content-Type: application/json" \
  --request POST \
  --data '{"name":"App Token for staging app" }' \
  "https://<herd-api.com>/v0/apps/a64d162d-a100-4a99-b722-342730214000/appTokens"
```

Example of response

```json
{
  "data": {
    "id": "52d87c23-4078-479a-9ff8-124d8000ad6d",
    "createdAt": "2019-09-02T09:41:20.588Z",
    "updatedAt": "2019-09-02T09:41:20.588Z",
    "name": "Private Token for staging app",
    "token": "GfT_pumBHWTdIvj20sXtDE8N~0sJtVlCVpUHvvqU"
  }
}
```

### Delete an app token

```
DELETE /v0/apps/:appId/appTokens/:appTokenId
```

| Attribute        | Type    | Required | Description         |
| ---------------- | ------- | -------- | ------------------- |
| `appId`          | string  | yes      | id of an app |
| `privateTokenId` | string  | yes      | id of an app token |

```
curl --header "Authorization: Herd-Private-Token <your_private_token>" \
  --request DELETE \
  "https://<herd-api.com>/v0/apps/4f401154-7056-4e45-8705-bc80f5322da3/appTokens/c3f2db69-cbd9-4be9-af8f-8c094a605cb3
```

Example of response

```json
{ }
```


# Status codes

The following table gives an overview of how the API functions generally behave.

| Request type  | Description |
| ------------- | ----------- |
| `GET` / `PUT` | Return `200 OK` if the resource is accessed or modified successfully. The (modified) result is returned as JSON. |
| `POST`        | Return `201 Created` if the resource is successfully created and return the newly created resource as JSON. |
| `DELETE`      | Returns `200` if the resource was deleted successfully. TODO: should return `204 No Content` |

The following table shows the possible return codes for API requests.

| Return values      | Description |
| ------------------ | ----------- |
| `200 OK`           | The `GET`, `PUT` or `DELETE` request was successful, the resource(s) itself is returned as JSON. |
| `204 No Content`   | The server has successfully fulfilled the request and that there is no additional content to send in the response payload body. |
| `201 Created`      | The `POST` request was successful and the resource is returned as JSON. |
| `304 Not Modified` | Indicates that the resource has not been modified since the last request. |
| `400 Bad Request`  | A required attribute of the API request is missing or malformed. |
| `401 Unauthorized` | The user is not authenticated, a valid JWT, Private Token or App Token is missing. |
| `403 Forbidden`    | The user is authenticated but is not permitted to access the resource(s). |
| `404 Not Found`    | A resource could not be accessed, e.g., an ID for a resource could not be found. |
| `500 Server Error` | While handling the request something went wrong server-side. |


```
{
  error: {
    "code": 401,
    "message": "authorization required"
  }
}
```

```
{
  error: {
    "code": 400,
    "message": "title is required"
  }
}
```

```
{
  error: {
    "code": 404,
    "message": "no such post found"
  }
}
```

